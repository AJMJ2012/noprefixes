﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System;
using Terraria.ModLoader;
using Terraria.Utilities;
using Terraria;

namespace NoPrefixes {
	public class NoPrefixes : Mod {
		public NoPrefixes() {
			Properties = new ModProperties {
				Autoload = true
			};
		}
		
		// The stuff I have to go through just to be able to keep the item's prefix for when this mod is unloaded
		public override void Load() {
			Settings.AllowReforge = false;
			On.Terraria.Item.Prefix += (Prefix, item, pre) => {
				if (Main.rand == null) { Main.rand = new UnifiedRandom(); }
				if (pre == 0 || item.type == 0) { return false; }
				UnifiedRandom unifiedRandom = WorldGen.gen ? WorldGen.genRand : Main.rand;
				int num = pre;
				bool flag = true;
				while (flag) {
					flag = false;
					if (num == -1 && unifiedRandom.Next(4) == 0) { num = 0; }
					if (pre < -1) { num = -1; }
					if (num == -1 || num == -2 || num == -3) {
						int num9 = ItemLoader.ChoosePrefix(item, unifiedRandom);
						if (num9 >= 0) { num = num9; }
						else if (item.type == 1 || item.type == 4 || item.type == 6 || item.type == 7 || item.type == 10 || item.type == 24 || item.type == 45 || item.type == 46 || item.type == 65 || item.type == 103 || item.type == 104 || item.type == 121 || item.type == 122 || item.type == 155 || item.type == 190 || item.type == 196 || item.type == 198 || item.type == 199 || item.type == 200 || item.type == 201 || item.type == 202 || item.type == 203 || item.type == 204 || item.type == 213 || item.type == 217 || item.type == 273 || item.type == 367 || item.type == 368 || item.type == 426 || item.type == 482 || item.type == 483 || item.type == 484 || item.type == 653 || item.type == 654 || item.type == 656 || item.type == 657 || item.type == 659 || item.type == 660 || item.type == 671 || item.type == 672 || item.type == 674 || item.type == 675 || item.type == 676 || item.type == 723 || item.type == 724 || item.type == 757 || item.type == 776 || item.type == 777 || item.type == 778 || item.type == 787 || item.type == 795 || item.type == 797 || item.type == 798 || item.type == 799 || item.type == 881 || item.type == 882 || item.type == 921 || item.type == 922 || item.type == 989 || item.type == 990 || item.type == 991 || item.type == 992 || item.type == 993 || item.type == 1123 || item.type == 1166 || item.type == 1185 || item.type == 1188 || item.type == 1192 || item.type == 1195 || item.type == 1199 || item.type == 1202 || item.type == 1222 || item.type == 1223 || item.type == 1224 || item.type == 1226 || item.type == 1227 || item.type == 1230 || item.type == 1233 || item.type == 1234 || item.type == 1294 || item.type == 1304 || item.type == 1305 || item.type == 1306 || item.type == 1320 || item.type == 1327 || item.type == 1506 || item.type == 1507 || item.type == 1786 || item.type == 1826 || item.type == 1827 || item.type == 1909 || item.type == 1917 || item.type == 1928 || item.type == 2176 || item.type == 2273 || item.type == 2608 || item.type == 2341 || item.type == 2330 || item.type == 2320 || item.type == 2516 || item.type == 2517 || item.type == 2746 || item.type == 2745 || item.type == 3063 || item.type == 3018 || item.type == 3211 || item.type == 3013 || item.type == 3258 || item.type == 3106 || item.type == 3065 || item.type == 2880 || item.type == 3481 || item.type == 3482 || item.type == 3483 || item.type == 3484 || item.type == 3485 || item.type == 3487 || item.type == 3488 || item.type == 3489 || item.type == 3490 || item.type == 3491 || item.type == 3493 || item.type == 3494 || item.type == 3495 || item.type == 3496 || item.type == 3497 || item.type == 3498 || item.type == 3500 || item.type == 3501 || item.type == 3502 || item.type == 3503 || item.type == 3504 || item.type == 3505 || item.type == 3506 || item.type == 3507 || item.type == 3508 || item.type == 3509 || item.type == 3511 || item.type == 3512 || item.type == 3513 || item.type == 3514 || item.type == 3515 || item.type == 3517 || item.type == 3518 || item.type == 3519 || item.type == 3520 || item.type == 3521 || item.type == 3522 || item.type == 3523 || item.type == 3524 || item.type == 3525 || (item.type >= 3462 && item.type <= 3466) || (item.type >= 2772 && item.type <= 2786) || (item.type == 3349 || item.type == 3352 || item.type == 3351 || (item.type >= 3764 && item.type <= 3769)) || item.type == 3772 || item.type == 3823 || item.type == 3827 || (bool)typeof(ItemLoader).GetTypeInfo().GetMethod("MeleePrefix", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, new object[] { item })) {
							int num10 = unifiedRandom.Next(40);
							if (num10 == 0) { num = 1; }
							if (num10 == 1) { num = 2; }
							if (num10 == 2) { num = 3; }
							if (num10 == 3) { num = 4; }
							if (num10 == 4) { num = 5; }
							if (num10 == 5) { num = 6; }
							if (num10 == 6) { num = 7; }
							if (num10 == 7) { num = 8; }
							if (num10 == 8) { num = 9; }
							if (num10 == 9) { num = 10; }
							if (num10 == 10) { num = 11; }
							if (num10 == 11) { num = 12; }
							if (num10 == 12) { num = 13; }
							if (num10 == 13) { num = 14; }
							if (num10 == 14) { num = 15; }
							if (num10 == 15) { num = 36; }
							if (num10 == 16) { num = 37; }
							if (num10 == 17) { num = 38; }
							if (num10 == 18) { num = 53; }
							if (num10 == 19) { num = 54; }
							if (num10 == 20) { num = 55; }
							if (num10 == 21) { num = 39; }
							if (num10 == 22) { num = 40; }
							if (num10 == 23) { num = 56; }
							if (num10 == 24) { num = 41; }
							if (num10 == 25) { num = 57; }
							if (num10 == 26) { num = 42; }
							if (num10 == 27) { num = 43; }
							if (num10 == 28) { num = 44; }
							if (num10 == 29) { num = 45; }
							if (num10 == 30) { num = 46; }
							if (num10 == 31) { num = 47; }
							if (num10 == 32) { num = 48; }
							if (num10 == 33) { num = 49; }
							if (num10 == 34) { num = 50; }
							if (num10 == 35) { num = 51; }
							if (num10 == 36) { num = 59; }
							if (num10 == 37) { num = 60; }
							if (num10 == 38) { num = 61; }
							if (num10 == 39) { num = 81; }
							int arg_C2F_2 = 40;
							PrefixCategory[] expr_C2B = new PrefixCategory[2];
							expr_C2B[0] = PrefixCategory.AnyWeapon;
							object[] arguments = new object[] { item, num, arg_C2F_2, expr_C2B };
							typeof(ModPrefix).GetTypeInfo().GetMethod("Roll", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, arguments);
							num = (int)arguments[1];
						}
						else if (item.type == 162 || item.type == 160 || item.type == 163 || item.type == 220 || item.type == 274 || item.type == 277 || item.type == 280 || item.type == 383 || item.type == 384 || item.type == 385 || item.type == 386 || item.type == 387 || item.type == 388 || item.type == 389 || item.type == 390 || item.type == 406 || item.type == 537 || item.type == 550 || item.type == 579 || item.type == 756 || item.type == 759 || item.type == 801 || item.type == 802 || item.type == 1186 || item.type == 1189 || item.type == 1190 || item.type == 1193 || item.type == 1196 || item.type == 1197 || item.type == 1200 || item.type == 1203 || item.type == 1204 || item.type == 1228 || item.type == 1231 || item.type == 1232 || item.type == 1259 || item.type == 1262 || item.type == 1297 || item.type == 1314 || item.type == 1325 || item.type == 1947 || item.type == 2332 || item.type == 2331 || item.type == 2342 || item.type == 2424 || item.type == 2611 || item.type == 2798 || item.type == 3012 || item.type == 3473 || item.type == 3098 || item.type == 3368 || item.type == 3835 || item.type == 3836 || item.type == 3858 || (bool)typeof(ItemLoader).GetTypeInfo().GetMethod("WeaponPrefix", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, new object[] { item })) {
							int num11 = unifiedRandom.Next(14);
							if (num11 == 0) { num = 36; }
							if (num11 == 1) { num = 37; }
							if (num11 == 2) { num = 38; }
							if (num11 == 3) { num = 53; }
							if (num11 == 4) { num = 54; }
							if (num11 == 5) { num = 55; }
							if (num11 == 6) { num = 39; }
							if (num11 == 7) { num = 40; }
							if (num11 == 8) { num = 56; }
							if (num11 == 9) { num = 41; }
							if (num11 == 10) { num = 57; }
							if (num11 == 11) { num = 59; }
							if (num11 == 12) { num = 60; }
							if (num11 == 13) { num = 61; }
							object[] arguments = new object[] { item, num, 14, new PrefixCategory[] { PrefixCategory.AnyWeapon } };
							typeof(ModPrefix).GetTypeInfo().GetMethod("Roll", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, arguments);
							num = (int)arguments[1];
						}
						else if (item.type == 39 || item.type == 44 || item.type == 95 || item.type == 96 || item.type == 98 || item.type == 99 || item.type == 120 || item.type == 164 || item.type == 197 || item.type == 219 || item.type == 266 || item.type == 281 || item.type == 434 || item.type == 435 || item.type == 436 || item.type == 481 || item.type == 506 || item.type == 533 || item.type == 534 || item.type == 578 || item.type == 655 || item.type == 658 || item.type == 661 || item.type == 679 || item.type == 682 || item.type == 725 || item.type == 758 || item.type == 759 || item.type == 760 || item.type == 796 || item.type == 800 || item.type == 905 || item.type == 923 || item.type == 964 || item.type == 986 || item.type == 1156 || item.type == 1187 || item.type == 1194 || item.type == 1201 || item.type == 1229 || item.type == 1254 || item.type == 1255 || item.type == 1258 || item.type == 1265 || item.type == 1319 || item.type == 1553 || item.type == 1782 || item.type == 1784 || item.type == 1835 || item.type == 1870 || item.type == 1910 || item.type == 1929 || item.type == 1946 || item.type == 2223 || item.type == 2269 || item.type == 2270 || item.type == 2624 || item.type == 2515 || item.type == 2747 || item.type == 2796 || item.type == 2797 || item.type == 3052 || item.type == 2888 || item.type == 3019 || item.type == 3029 || item.type == 3007 || item.type == 3008 || item.type == 3210 || item.type == 3107 || item.type == 3245 || item.type == 3475 || item.type == 3540 || item.type == 3854 || item.type == 3859 || item.type == 3821 || item.type == 3480 || item.type == 3486 || item.type == 3492 || item.type == 3498 || item.type == 3504 || item.type == 3510 || item.type == 3516 || item.type == 3350 || item.type == 3546 || item.type == 3788 || (bool)typeof(ItemLoader).GetTypeInfo().GetMethod("RangedPrefix", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, new object[] { item })) {
							int num12 = unifiedRandom.Next(36);
							if (num12 == 0) { num = 16; }
							if (num12 == 1) { num = 17; }
							if (num12 == 2) { num = 18; }
							if (num12 == 3) { num = 19; }
							if (num12 == 4) { num = 20; }
							if (num12 == 5) { num = 21; }
							if (num12 == 6) { num = 22; }
							if (num12 == 7) { num = 23; }
							if (num12 == 8) { num = 24; }
							if (num12 == 9) { num = 25; }
							if (num12 == 10) { num = 58; }
							if (num12 == 11) { num = 36; }
							if (num12 == 12) { num = 37; }
							if (num12 == 13) { num = 38; }
							if (num12 == 14) { num = 53; }
							if (num12 == 15) { num = 54; }
							if (num12 == 16) { num = 55; }
							if (num12 == 17) { num = 39; }
							if (num12 == 18) { num = 40; }
							if (num12 == 19) { num = 56; }
							if (num12 == 20) { num = 41; }
							if (num12 == 21) { num = 57; }
							if (num12 == 22) { num = 42; }
							if (num12 == 23) { num = 43; }
							if (num12 == 24) { num = 44; }
							if (num12 == 25) { num = 45; }
							if (num12 == 26) { num = 46; }
							if (num12 == 27) { num = 47; }
							if (num12 == 28) { num = 48; }
							if (num12 == 29) { num = 49; }
							if (num12 == 30) { num = 50; }
							if (num12 == 31) { num = 51; }
							if (num12 == 32) { num = 59; }
							if (num12 == 33) { num = 60; }
							if (num12 == 34) { num = 61; }
							if (num12 == 35) { num = 82; }
							object[] arguments = new object[] { item, num, 36, new PrefixCategory[] { PrefixCategory.AnyWeapon, PrefixCategory.Ranged } };
							typeof(ModPrefix).GetTypeInfo().GetMethod("Roll", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, arguments);
							num = (int)arguments[1];
						}
						else if (item.type == 64 || item.type == 112 || item.type == 113 || item.type == 127 || item.type == 157 || item.type == 165 || item.type == 218 || item.type == 272 || item.type == 494 || item.type == 495 || item.type == 496 || item.type == 514 || item.type == 517 || item.type == 518 || item.type == 519 || item.type == 683 || item.type == 726 || item.type == 739 || item.type == 740 || item.type == 741 || item.type == 742 || item.type == 743 || item.type == 744 || item.type == 788 || item.type == 1121 || item.type == 1155 || item.type == 1157 || item.type == 1178 || item.type == 1244 || item.type == 1256 || item.type == 1260 || item.type == 1264 || item.type == 1266 || item.type == 1295 || item.type == 1296 || item.type == 1308 || item.type == 1309 || item.type == 1313 || item.type == 1336 || item.type == 1444 || item.type == 1445 || item.type == 1446 || item.type == 1572 || item.type == 1801 || item.type == 1802 || item.type == 1930 || item.type == 1931 || item.type == 2188 || item.type == 2622 || item.type == 2621 || item.type == 2584 || item.type == 2551 || item.type == 2366 || item.type == 2535 || item.type == 2365 || item.type == 2364 || item.type == 2623 || item.type == 2750 || item.type == 2795 || item.type == 3053 || item.type == 3051 || item.type == 3209 || item.type == 3014 || item.type == 3105 || item.type == 2882 || item.type == 3269 || item.type == 3006 || item.type == 3377 || item.type == 3069 || item.type == 2749 || item.type == 3249 || item.type == 3476 || item.type == 3474 || item.type == 3531 || item.type == 3541 || item.type == 3542 || item.type == 3569 || item.type == 3570 || item.type == 3571 || item.type == 3779 || item.type == 3787 || item.type == 3531 || item.type == 3852 || item.type == 3870 || item.type == 3824 || item.type == 3818 || item.type == 3829 || item.type == 3832 || item.type == 3825 || item.type == 3819 || item.type == 3830 || item.type == 3833 || item.type == 3826 || item.type == 3820 || item.type == 3831 || item.type == 3834 || (bool)typeof(ItemLoader).GetTypeInfo().GetMethod("MagicPrefix", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, new object[] { item })) {
							int num13 = unifiedRandom.Next(36);
							if (num13 == 0) { num = 26; }
							if (num13 == 1) { num = 27; }
							if (num13 == 2) { num = 28; }
							if (num13 == 3) { num = 29; }
							if (num13 == 4) { num = 30; }
							if (num13 == 5) { num = 31; }
							if (num13 == 6) { num = 32; }
							if (num13 == 7) { num = 33; }
							if (num13 == 8) { num = 34; }
							if (num13 == 9) { num = 35; }
							if (num13 == 10) { num = 52; }
							if (num13 == 11) { num = 36; }
							if (num13 == 12) { num = 37; }
							if (num13 == 13) { num = 38; }
							if (num13 == 14) { num = 53; }
							if (num13 == 15) { num = 54; }
							if (num13 == 16) { num = 55; }
							if (num13 == 17) { num = 39; }
							if (num13 == 18) { num = 40; }
							if (num13 == 19) { num = 56; }
							if (num13 == 20) { num = 41; }
							if (num13 == 21) { num = 57; }
							if (num13 == 22) { num = 42; }
							if (num13 == 23) { num = 43; }
							if (num13 == 24) { num = 44; }
							if (num13 == 25) { num = 45; }
							if (num13 == 26) { num = 46; }
							if (num13 == 27) { num = 47; }
							if (num13 == 28) { num = 48; }
							if (num13 == 29) { num = 49; }
							if (num13 == 30) { num = 50; }
							if (num13 == 31) { num = 51; }
							if (num13 == 32) { num = 59; }
							if (num13 == 33) { num = 60; }
							if (num13 == 34) { num = 61; }
							if (num13 == 35) { num = 83; }
							object[] arguments = new object[] { item, num, 36, new PrefixCategory[] { PrefixCategory.AnyWeapon, PrefixCategory.Magic } };
							typeof(ModPrefix).GetTypeInfo().GetMethod("Roll", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, arguments);
							num = (int)arguments[1];
						}
						else if (item.type == 55 || item.type == 119 || item.type == 191 || item.type == 284 || item.type == 670 || item.type == 1122 || item.type == 1513 || item.type == 1569 || item.type == 1571 || item.type == 1825 || item.type == 1918 || item.type == 3054 || item.type == 3262 || (item.type >= 3278 && item.type <= 3292) || (item.type >= 3315 && item.type <= 3317) || item.type == 3389 || item.type == 3030 || item.type == 3543 || (bool)typeof(ItemLoader).GetTypeInfo().GetMethod("WeaponPrefix", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, new object[] { item })) {
							int num14 = unifiedRandom.Next(14);
							if (num14 == 0) { num = 36; }
							if (num14 == 1) { num = 37; }
							if (num14 == 2) { num = 38; }
							if (num14 == 3) { num = 53; }
							if (num14 == 4) { num = 54; }
							if (num14 == 5) { num = 55; }
							if (num14 == 6) { num = 39; }
							if (num14 == 7) { num = 40; }
							if (num14 == 8) { num = 56; }
							if (num14 == 9) { num = 41; }
							if (num14 == 10) { num = 57; }
							if (num14 == 11) { num = 59; }
							if (num14 == 12) { num = 60; }
							if (num14 == 13) { num = 61; }
							object[] arguments = new object[] { item, num, 14, new PrefixCategory[] { PrefixCategory.AnyWeapon } };
							typeof(ModPrefix).GetTypeInfo().GetMethod("Roll", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, arguments);
							num = (int)arguments[1];
						}
						else {
							if (!item.accessory || item.type == 267 || item.type == 562 || item.type == 563 || item.type == 564 || item.type == 565 || item.type == 566 || item.type == 567 || item.type == 568 || item.type == 569 || item.type == 570 || item.type == 571 || item.type == 572 || item.type == 573 || item.type == 574 || item.type == 576 || item.type == 1307 || (item.type >= 1596 && item.type < 1610) || item.vanity) {
								return false;
							}
							num = unifiedRandom.Next(62, 81);
							object[] arguments = new object[] { item, num, 19, new PrefixCategory[] { PrefixCategory.Accessory } };
							typeof(ModPrefix).GetTypeInfo().GetMethod("Roll", BindingFlags.NonPublic | BindingFlags.Static).Invoke(null, arguments);
							num = (int)arguments[1];
						}
					}
					if (pre == -3) { return true; }
					if (num >= 84) {
						ModPrefix expr_27CA = ModPrefix.GetPrefix((byte)num);
						if (expr_27CA != null) {
							expr_27CA.ValidateItem(item, ref flag);
						}
					}
					if (pre == -2 && num == 0) {
						num = -1;
						flag = true;
					}
				}
				if (num >= 84) {
					ModPrefix expr_28A5 = ModPrefix.GetPrefix((byte)num);
					if (expr_28A5 != null) {
						expr_28A5.Apply(item);
					}
				}
				item.prefix = (byte)num;
				return true;
			};
			
			On.Terraria.Item.AffixName += (AffixName, item) => {
				return item.Name;
			};
		}
		
		public override void PostUpdateInput() {
			if (Main.InReforgeMenu && !Settings.AllowReforge) {
				Main.NewText("Reforging has been diabled by " + this.DisplayName);
				Main.InReforgeMenu = false;
			}
		}
	}

	public static class Settings {
		public static bool AllowReforge = false;
	}
	
	public class GNPC : GlobalNPC {
		public override bool PreChatButtonClicked(NPC npc, bool firstButton) {
			if (npc.type == 107 && !firstButton && !Settings.AllowReforge) {
				Main.NewText("Reforging has been diabled by " + mod.DisplayName);
				return false;
			}
			return base.PreChatButtonClicked(npc, firstButton);
		}
	}

	public class GItem : GlobalItem {
		public override bool InstancePerEntity { get { return true; } }
		public override bool CloneNewInstances { get { return true; } }
		
		// Make it so cheaters can't load this mod and blindly reforge their armor for much cheaper by keeping the item's original prefix
		public byte lastPrefix = 0;
		public override bool NewPreReforge(Item item) {
			lastPrefix = item.prefix;
			return base.NewPreReforge(item);
		}
		
		public override void PostReforge(Item item) {
			item.prefix = lastPrefix;
		}
		
		public override void ModifyTooltips(Item item, List<TooltipLine> tooltips) {
			List<string> DeleteTooltips = new List<string> {
				"PrefixDamage",
				"PrefixSpeed",
				"PrefixCritChance",
				"PrefixUseMana",
				"PrefixSize",
				"PrefixShootSpeed",
				"PrefixKnockback",
				"PrefixAccDefense",
				"PrefixAccMaxMana",
				"PrefixAccCritChance",
				"PrefixAccDamage",
				"PrefixAccMoveSpeed",
				"PrefixAccMeleeSpeed",
			};
			if (item.prefix != 255) {
				for (int i=0; i < tooltips.Count; i++) {
					if (tooltips[i].mod == "Terraria" && DeleteTooltips.Any(s => s == (tooltips[i].Name))) {
						tooltips.RemoveAt(i);
						i = -1;
						continue;
					}
				}
			}
		}
		
		public override void UpdateEquip(Item item, Player player) {
			if (item.prefix == 62) {
				player.statDefense--;
			}
			else if (item.prefix == 63) {
				player.statDefense -= 2;
			}
			else if (item.prefix == 64) {
				player.statDefense -= 3;
			}
			else if (item.prefix == 65) {
				player.statDefense -= 4;
			}
			else if (item.prefix == 66) {
				player.statManaMax2 -= 20;
			}
			else if (item.prefix == 67) {
				player.meleeCrit -= 2;
				player.rangedCrit -= 2;
				player.magicCrit -= 2;
				player.thrownCrit -= 2;
			}
			else if (item.prefix == 68) {
				player.meleeCrit -= 4;
				player.rangedCrit -= 4;
				player.magicCrit -= 4;
				player.thrownCrit -= 4;
			}
			else if (item.prefix == 69) {
				player.meleeDamage -= 0.01f;
				player.rangedDamage -= 0.01f;
				player.magicDamage -= 0.01f;
				player.minionDamage -= 0.01f;
				player.thrownDamage -= 0.01f;
			}
			else if (item.prefix == 70) {
				player.meleeDamage -= 0.02f;
				player.rangedDamage -= 0.02f;
				player.magicDamage -= 0.02f;
				player.minionDamage -= 0.02f;
				player.thrownDamage -= 0.02f;
			}
			else if (item.prefix == 71) {
				player.meleeDamage -= 0.03f;
				player.rangedDamage -= 0.03f;
				player.magicDamage -= 0.03f;
				player.minionDamage -= 0.03f;
				player.thrownDamage -= 0.03f;
			}
			else if (item.prefix == 72) {
				player.meleeDamage -= 0.04f;
				player.rangedDamage -= 0.04f;
				player.magicDamage -= 0.04f;
				player.minionDamage -= 0.04f;
				player.thrownDamage -= 0.04f;
			}
			else if (item.prefix == 73) {
				player.moveSpeed -= 0.01f;
			}
			else if (item.prefix == 74) {
				player.moveSpeed -= 0.02f;
			}
			else if (item.prefix == 75) {
				player.moveSpeed -= 0.03f;
			}
			else if (item.prefix == 76) {
				player.moveSpeed -= 0.04f;
			}
			else if (item.prefix == 77) {
				player.meleeSpeed -= 0.01f;
			}
			else if (item.prefix == 78) {
				player.meleeSpeed -= 0.02f;
			}
			else if (item.prefix == 79) {
				player.meleeSpeed -= 0.03f;
			}
			else if (item.prefix == 80) {
				player.meleeSpeed -= 0.04f;
			}
			else if (item.prefix > 0) { // Remove modded ones because unsupported.
				item.prefix = 0;
			}
		}
	}
}
